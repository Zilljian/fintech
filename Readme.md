# fintech
  
This project contains multiple subprojects for demonstration purposes.  
You can change any application configurations in `resources` folder.

## plain-servlet-demo
  
Demonstrate usage of plain servlet approach. Servlets are deployed on Tomcat servlet container.

## spring-demo

Demonstrate usage of Spring Framework + Spring MVC module. Servlets are deployed on Tomcat servlet container.

Project supports 2 profiles - `dev` and `test`.  
For example, set profile through VM options in Run Configurations.  
Add `-Dspring.profiles.active=` option.

## spring-boot-demo

Demonstrate usage of Spring Boot + Spring Boot Web module. By default, it's deployed on Tomcat servlet container.

Project supports 2 profiles - `dev` and `test`.  
For example, set profile through VM options in Run Configurations.  
Add `-Dspring.profiles.active=` option.

## spring-mvc-demo
Demonstrate usage of Spring MVC through Spring Boot Web module. This demo generates swagger schema, that's available on `/swagger` path. Swagger UI 
additionally available at `/swagger.html`.

## spring-testing-demo
Demonstrate different testing approaches in Spring using MockMVC, Mockito and Testcontainers. 
This demo generates swagger schema, that's available on `/swagger` path. 
Swagger UI additionally available at `/swagger.html`.

In order to start up application change database url and credentials in `application.yaml`.
No need do this for running tests.