package ru.tinkoff.fintech.java.concurrency.demo;

import org.junit.jupiter.api.Test;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class TestConcurrency {

    @Test
    void test() throws InterruptedException {
        var countDownLatch = new CountDownLatch(4);
        new Thread(() -> {
            while (true) {
                System.out.println("thread-1");
                countDownLatch.countDown();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        new Thread(() -> {
            while (true) {
                System.out.println("thread-2");
                countDownLatch.countDown();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        //Runtime.getRuntime().addShutdownHook(new Thread(countDownLatch::countDown));
        countDownLatch.await();
        //Runtime.getRuntime().exit(0);
    }

    @Test
    void test2() throws ExecutionException, InterruptedException, TimeoutException {
        CompletableFuture
            .supplyAsync(this::process)
            .thenCombineAsync(CompletableFuture.supplyAsync(this::process2),
                              (r1, r2) -> {
                                  System.out.println(r1);
                                  System.out.println(r2);
                                  return CompletableFuture.completedFuture("Task is done");
                              })
            .get(6, TimeUnit.SECONDS);
    }

    private String process() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "Process 1 has been done";
    }

    private String process2() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "Process 2 has been done";
    }
}
