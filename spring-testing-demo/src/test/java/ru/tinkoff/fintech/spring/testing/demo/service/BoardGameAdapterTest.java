package ru.tinkoff.fintech.spring.testing.demo.service;

import org.junit.jupiter.api.Test;
import ru.tinkoff.fintech.spring.testing.demo.model.BoardGame;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class BoardGameAdapterTest {

    private final BoardGameService boardGameService = mock(BoardGameService.class);
    private final BoardGameAdapter boardGameAdapter = new BoardGameAdapter(boardGameService);

    @Test
    void test() {
        when(boardGameService.select("test"))
            .thenReturn(prepareBoardGame());

        var result = boardGameAdapter.getByName("test");

        assertEquals(prepareBoardGame(), result);
    }

    private BoardGame prepareBoardGame() {
        return new BoardGame(
            "test",
            "1-4",
            240,
            8.5f,
            ""
        );
    }
}