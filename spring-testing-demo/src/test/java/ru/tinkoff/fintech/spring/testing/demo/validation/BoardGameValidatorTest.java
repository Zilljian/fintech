package ru.tinkoff.fintech.spring.testing.demo.validation;

import org.junit.jupiter.api.Test;
import ru.tinkoff.fintech.spring.testing.demo.model.BoardGame;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class BoardGameValidatorTest {

    private final BoardGameValidator boardGameValidator = new BoardGameValidator();

    @Test
    void testValid() {
        assertTrue(boardGameValidator.isValid(prepareValidBoardGame(), null));
    }

    @Test
    void testInvalidName() {
        assertFalse(boardGameValidator.isValid(prepareInvalidNameBoardGame(), null));
    }

    @Test
    void testInvalidPlayers() {
        assertFalse(boardGameValidator.isValid(prepareInvalidPlayersBoardGame(), null));
    }

    private BoardGame prepareValidBoardGame() {
        return new BoardGame(
            "test",
            "1-4",
            240,
            8.5f,
            ""
        );
    }

    private BoardGame prepareInvalidNameBoardGame() {
        return new BoardGame(
            "",
            "1-4",
            240,
            8.5f,
            ""
        );
    }

    private BoardGame prepareInvalidPlayersBoardGame() {
        return new BoardGame(
            "test",
            "1",
            240,
            8.5f,
            ""
        );
    }
}