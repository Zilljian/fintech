package ru.tinkoff.fintech.spring.testing.demo.logic;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.web.servlet.MockMvc;
import ru.tinkoff.fintech.spring.testing.demo.AbstractTest;
import ru.tinkoff.fintech.spring.testing.demo.exception.ApplicationError.ApplicationException.ApplicationExceptionCompanion;
import ru.tinkoff.fintech.spring.testing.demo.model.BoardGame;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.tinkoff.fintech.spring.testing.demo.exception.ApplicationError.BOARD_GAME_ALREADY_EXISTS;
import static ru.tinkoff.fintech.spring.testing.demo.exception.ApplicationError.BOARD_GAME_DOES_NOT_EXIST;

@AutoConfigureMockMvc
public class BoardGameApplicationTest extends AbstractTest {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private MockMvc mockMvc;

    private final ObjectMapper jackson = new ObjectMapper();

    @Test
    void testAddNewSuccess() throws Exception {
        var boardGame = prepareBoardGame("Rising Sun");
        var boardGameJson = jackson.writeValueAsString(boardGame);

        mockMvc.perform(post("/boardgames/")
                            .contentType("application/json")
                            .content(boardGameJson))
            .andExpect(status().isOk());

        var dbEntry = jdbcTemplate.queryForObject("select * from public.boardgame where name = 'Rising Sun'", BoardGame.mapper);
        assertEquals(boardGame, dbEntry);
    }

    @Test
    void testAddNewAlreadyExists() throws Exception {
        var boardGame = prepareBoardGame("Rising Sun");
        var boardGameJson = jackson.writeValueAsString(boardGame);
        var alreadyExistsException = jackson.writeValueAsString(prepareBoardGameAlreadyExistsExceptionCompanion());

        mockMvc.perform(post("/boardgames/")
                            .contentType("application/json")
                            .content(boardGameJson))
            .andExpect(status().isOk());

        mockMvc.perform(post("/boardgames/")
                            .contentType("application/json")
                            .content(boardGameJson))
            .andDo(print())
            .andExpect(content().string(alreadyExistsException))
            .andExpect(status().is(BOARD_GAME_ALREADY_EXISTS.code));

        var dbEntry = jdbcTemplate.queryForObject("select * from public.boardgame where name = 'Rising Sun'", BoardGame.mapper);
        assertEquals(boardGame, dbEntry);
    }

    @Test
    void testGetSuccess() throws Exception {
        var boardGame = prepareBoardGame("Rising Sun");
        populateDb(boardGame);
        var boardGameJson = jackson.writeValueAsString(boardGame);

        mockMvc.perform(get("/boardgames/")
                            .param("name", "Rising Sun"))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().string(boardGameJson));
    }

    @Test
    void testGetDoesNotExist() throws Exception {
        var doesNotExist = jackson.writeValueAsString(prepareBoardGameDoesNotExistExceptionCompanion());

        mockMvc.perform(get("/boardgames/")
                            .param("name", "Rising Sun"))
            .andDo(print())
            .andExpect(status().is(BOARD_GAME_DOES_NOT_EXIST.code))
            .andExpect(content().string(doesNotExist));
    }

    private ApplicationExceptionCompanion prepareBoardGameAlreadyExistsExceptionCompanion() {
        return BOARD_GAME_ALREADY_EXISTS.exception("Board game with name = Rising Sun already exists").companion;
    }

    private ApplicationExceptionCompanion prepareBoardGameDoesNotExistExceptionCompanion() {
        return BOARD_GAME_DOES_NOT_EXIST.exception("Board game with name = Rising Sun does not exist").companion;
    }

    private void populateDb(BoardGame boardGame) {
        jdbcTemplate.update("""
                            INSERT INTO public.boardgame (name, players, play_time, rating, description)
                            VALUES (?, ?, ?, ?, ?)
                            """, ps -> {
            ps.setString(1, boardGame.name());
            ps.setString(2, boardGame.players());
            ps.setInt(3, boardGame.playTime());
            ps.setFloat(4, boardGame.rating());
            ps.setString(5, boardGame.description());
        });
    }

    private BoardGame prepareBoardGame(String name) {
        return new BoardGame(
            name,
            "1-4",
            240,
            8.5f,
            ""
        );
    }
}
