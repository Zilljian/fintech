package ru.tinkoff.fintech.spring.testing.demo.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import ru.tinkoff.fintech.spring.testing.demo.model.BoardGame;

import static java.lang.String.format;
import static ru.tinkoff.fintech.spring.testing.demo.exception.ApplicationError.BOARD_GAME_DOES_NOT_EXIST;

@Component
public class BoardGameAdapter {

    public static final Logger log = LoggerFactory.getLogger(BoardGameAdapter.class);

    private final BoardGameService boardGameService;

    public BoardGameAdapter(BoardGameService boardGameService) {
        this.boardGameService = boardGameService;
    }

    public void insertNew(BoardGame boardGame) {
        log.info("BoardGameAdapter.insertNew.in boardGame={}", boardGame);
        try {
            boardGameService.insert(boardGame);
        } catch (Exception e) {
            log.error("BoardGameAdapter.insertNew.thrown", e);
            throw e;
        }
        log.info("BoardGameAdapter.insertNew.out");
    }

    public BoardGame getByName(String name) {
        log.info("BoardGameAdapter.getByName.in name={}", name);
        try {
            var result = boardGameService.select(name);
            log.info("BoardGameAdapter.getByName.out result={}", result);
            return result;
        } catch (Exception e) {
            log.error("BoardGameAdapter.getByName.thrown", e);
            throw BOARD_GAME_DOES_NOT_EXIST.exception(format("Board game with name = %s does not exist", name));
        }
    }

    public boolean isExistByName(String name) {
        log.info("BoardGameAdapter.isExistByName.in name={}", name);
        var result = true;
        try {
            boardGameService.select(name);
        } catch (Exception e) {
            result = false;
        }
        log.info("BoardGameAdapter.isExistByName.out result={}", result);
        return result;
    }
}
