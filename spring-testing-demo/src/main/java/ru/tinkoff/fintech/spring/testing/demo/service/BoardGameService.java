package ru.tinkoff.fintech.spring.testing.demo.service;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import ru.tinkoff.fintech.spring.testing.demo.model.BoardGame;

import static java.lang.String.format;

@Service
public class BoardGameService {

    private final JdbcTemplate jdbcTemplate;

    public BoardGameService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void insert(BoardGame boardGame) {
        jdbcTemplate.update("""
                            INSERT INTO public.boardgame (name, players, play_time, rating, description)
                            VALUES (?, ?, ?, ?, ?)
                            """, ps -> {
            ps.setString(1, boardGame.name());
            ps.setString(2, boardGame.players());
            ps.setInt(3, boardGame.playTime());
            ps.setFloat(4, boardGame.rating());
            ps.setString(5, boardGame.description());
        });
    }

    public BoardGame select(String name) {
        var sql = format("""
                         SELECT * FROM public.boardgame
                         WHERE name = '%s'
                         """, name);
        return jdbcTemplate.queryForObject(sql, BoardGame.mapper);
    }
}
