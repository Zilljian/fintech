package ru.tinkoff.fintech.spring.testing.demo.exception;

import com.fasterxml.jackson.annotation.JsonIgnore;

public enum ApplicationError {

    BOARD_GAME_ALREADY_EXISTS("Board game already exists", 400),
    BOARD_GAME_DOES_NOT_EXIST("Board game does not exist", 400),
    ;

    public final String message;
    public final int code;

    ApplicationError(String message, int code) {
        this.message = message;
        this.code = code;
    }

    public ApplicationException exception(String args) {
        return new ApplicationException(this, args);
    }

    public static class ApplicationException extends RuntimeException {

        public final ApplicationExceptionCompanion companion;

        ApplicationException(ApplicationError error, String message) {
            super(error.message + " : " + message);
            this.companion = new ApplicationExceptionCompanion(error.code, error.message + " : " + message);
        }

        public static record ApplicationExceptionCompanion(@JsonIgnore int code, String message) {}
    }
}
