package ru.tinkoff.fintech.spring.testing.demo.controller;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.tinkoff.fintech.spring.testing.demo.exception.ApplicationError.ApplicationException;
import ru.tinkoff.fintech.spring.testing.demo.exception.ApplicationError.ApplicationException.ApplicationExceptionCompanion;
import ru.tinkoff.fintech.spring.testing.demo.model.BoardGame;
import ru.tinkoff.fintech.spring.testing.demo.service.BoardGameAdapter;

import javax.validation.Valid;

import static java.lang.String.format;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static ru.tinkoff.fintech.spring.testing.demo.exception.ApplicationError.BOARD_GAME_ALREADY_EXISTS;

@OpenAPIDefinition(
    info = @Info(
        title = "board-game-service",
        version = "1.0.0",
        description = "Service provides board game operations",
        contact = @Contact(
            name = "Balakin Ilya",
            email = "i.balakin@tinkoff.ru"
        )
    )
)
@RestController
public class BoardGameController {

    private static final Logger log = LoggerFactory.getLogger(BoardGameController.class);

    private final BoardGameAdapter boardGameAdapter;

    public BoardGameController(BoardGameAdapter boardGameAdapter) {
        this.boardGameAdapter = boardGameAdapter;
    }

    @PostMapping(
        consumes = APPLICATION_JSON_VALUE,
        path = "/boardgames/"
    )
    @Operation(
        description = "Add new board game to data base. Method checks name validity",
        summary = "Add new board game"
    )
    public void addBoardGame(@RequestBody @Valid BoardGame boardGame) {
        if (boardGameAdapter.isExistByName(boardGame.name())) {
            throw BOARD_GAME_ALREADY_EXISTS.exception(format("Board game with name = %s already exists", boardGame.name()));
        }
        boardGameAdapter.insertNew(boardGame);
        log.info("New game with name {} has been added!", boardGame.name());
    }

    @GetMapping(
        produces = APPLICATION_JSON_VALUE,
        path = "/boardgames/"
    )
    @Operation(
        description = "Get board game from data base",
        summary = "Get board game"
    )
    public BoardGame getBoardGame(@RequestParam("name") String name) {
        return boardGameAdapter.getByName(name);
    }

    @ExceptionHandler(ApplicationException.class)
    public ResponseEntity<ApplicationExceptionCompanion> handleApplicationException(ApplicationException e) {
        return ResponseEntity.status(e.companion.code()).body(e.companion);
    }
}
