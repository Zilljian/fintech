package ru.tinkoff.fintech.spring.testing.demo.model;

import org.springframework.jdbc.core.RowMapper;
import ru.tinkoff.fintech.spring.testing.demo.validation.BoardGameConstraint;

import java.sql.ResultSet;
import java.sql.SQLException;

@BoardGameConstraint
public record BoardGame(
    String name,
    String players,
    Integer playTime,
    Float rating,
    String description
) {

    public static Mapper mapper = new Mapper();

    private static class Mapper implements RowMapper<BoardGame> {

        @Override
        public BoardGame mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new BoardGame(
                rs.getString("name"),
                rs.getString("players"),
                rs.getInt("play_time"),
                rs.getFloat("rating"),
                rs.getString("description")
            );
        }
    }
}
