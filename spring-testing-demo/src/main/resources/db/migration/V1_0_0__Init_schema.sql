CREATE TABLE public.boardgame
(
    name        varchar,
    players     varchar,
    play_time   int,
    rating      float,
    description varchar
);