package ru.tinkoff.fintech.simple.cache.demo;

import org.junit.jupiter.api.Test;
import ru.tinkoff.fintech.simple.cache.demo.cache.LruCache;

import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class LruCacheTest {

    @Test
    void rewriteTest() {
        var cache = new LruCache<String>(2);

        Function<String, String> functionMock1 = mock(Function.class);
        Function<String, String> functionMock2 = mock(Function.class);
        Function<String, String> functionMock3 = mock(Function.class);
        Function<String, String> functionMock4 = mock(Function.class);

        var firstExpected = "1";
        var secondExpected = "2";
        var thirdExpected = "3";

        var firstResult = "1";
        var secondResult = "2";
        var secondResult2 = "22";
        var thirdResult = "3";

        when(functionMock1.apply(any())).thenReturn(firstResult);
        when(functionMock2.apply(any())).thenReturn(secondResult);
        when(functionMock3.apply(any())).thenReturn(secondResult2);
        when(functionMock4.apply(any())).thenReturn(thirdResult);

        assertEquals(firstExpected, cache.getOrElseCompute("1", functionMock1));
        assertEquals(firstExpected, cache.getOrElseCompute("1", functionMock1));
        assertEquals(1, cache.size());
        verify(functionMock1, times(1)).apply(any());

        assertEquals(secondExpected, cache.getOrElseCompute("2", functionMock2));
        assertEquals(secondExpected, cache.getOrElseCompute("2", functionMock3));
        assertEquals(2, cache.size());
        verify(functionMock2, times(1)).apply(any());
        verify(functionMock3, never()).apply(any());

        assertEquals(thirdExpected, cache.getOrElseCompute("3", functionMock4));
        assertEquals(secondExpected, cache.getOrElseCompute("2", functionMock2));
        assertEquals(2, cache.size());
        verify(functionMock2, times(1)).apply(any());
        verify(functionMock4, times(1)).apply(any());
    }

    @Test
    void multiThreadWriteTest() {
        var cache = new LruCache<String>(2);

        Function<String, String> functionMock1 = mock(Function.class);
        Function<String, String> functionMock2 = mock(Function.class);

        var firstExpected = "value 1";

        var firstResult = "value 1";
        var secondResult = "value 2";

        var thread1 = new Thread(() -> assertEquals(firstExpected, cache.getOrElseCompute("1", functionMock1)));
        var thread2 = new Thread(() -> assertEquals(firstExpected, cache.getOrElseCompute("1", functionMock2)));

        when(functionMock1.apply(any()))
            .thenAnswer(i -> {
                Thread.sleep(1000);
                return firstResult;
            });
        when(functionMock2.apply(any())).thenAnswer(i -> secondResult);

        thread1.run();
        thread2.run();

        verify(functionMock1, times(1)).apply(any());
        verify(functionMock2, never()).apply(any());
    }
}