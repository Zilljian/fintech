package ru.tinkoff.fintech.simple.cache.demo.service;

import org.springframework.stereotype.Component;
import ru.tinkoff.fintech.simple.cache.demo.model.Student;

import java.util.UUID;

@Component
public class StudentRepository {

    public Student create(String name, Integer age) {
        assert !name.isBlank();
        assert age > 0;
        return Student.of(name, age);
    }

    public void save(Student student) {
    }

    public void update(Student student) {
    }

    public Student find(String id) {
        return new Student(UUID.fromString(id), "random", 20);
    }

    public void remove(String id) {
    }
}
