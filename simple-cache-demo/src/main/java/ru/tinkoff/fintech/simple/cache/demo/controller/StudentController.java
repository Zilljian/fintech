package ru.tinkoff.fintech.simple.cache.demo.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ru.tinkoff.fintech.simple.cache.demo.service.StudentRepository;

@RestController
public class StudentController {

    private final ObjectMapper jackson;
    private final StudentRepository studentRepository;

    private final String defaultName;

    public StudentController(
        ObjectMapper jackson,
        StudentRepository studentRepository,
        @Value("${student.default-name}") String defaultName
    ) {
        this.jackson = jackson;
        this.studentRepository = studentRepository;
        this.defaultName = defaultName;
    }

    @GetMapping(
        value = "/student",
        produces = "application/json")
    @ResponseBody
    public String createStudent() throws JsonProcessingException {
        return jackson.writeValueAsString(studentRepository.create(defaultName, 20));
    }
}
