package ru.tinkoff.fintech.simple.cache.demo.cache.deque;

public class Node<T> {

    Node<T> previous;
    Node<T> next;
    public final T value;

    Node(Node<T> previous, Node<T> next, T value) {
        this.previous = previous;
        this.next = next;
        this.value = value;
    }

    static <T> Node<T> withValue(T value) {
        return new Node<T>(null, null, value);
    }
}
