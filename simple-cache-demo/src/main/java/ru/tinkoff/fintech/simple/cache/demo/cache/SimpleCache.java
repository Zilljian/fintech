package ru.tinkoff.fintech.simple.cache.demo.cache;

import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;
import java.util.function.Function;

public class SimpleCache<T> implements Cache<T> {

    private final ConcurrentHashMap<String, T> storage = new ConcurrentHashMap<>();

    public T getOrElseCompute(String id, Function<String, ? extends T> function) {
        return storage.computeIfAbsent(id, function);
    }

    public void runThenInvalidate(String id, Runnable runnable) {
        storage.compute(id, (key, value) -> {
            runnable.run();
            return null;
        });
    }

    public void consumeThenInvalidate(String id, Consumer<String> consumer) {
        storage.compute(id, (key, value) -> {
            consumer.accept(key);
            return null;
        });
    }
}
