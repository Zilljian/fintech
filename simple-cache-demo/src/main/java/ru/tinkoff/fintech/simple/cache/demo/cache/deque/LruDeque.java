package ru.tinkoff.fintech.simple.cache.demo.cache.deque;

public class LruDeque<T> {

    private Node<T> head;
    private Node<T> tail;
    private int size = 0;

    public Node<T> removeLeastUsed() {
        var oldTail = tail;
        var newTail = oldTail.previous;
        newTail.next = null;
        tail = newTail;
        size--;
        return oldTail;
    }

    public Node<T> remove(Node<T> node) {
        var previous = node.previous;
        if (previous != null) {
            previous.next = node.next;
        }
        node.previous = null;
        node.next = null;
        size--;
        return node;
    }

    public Node<T> putOnTop(T value) {
        var oldHead = head;
        head = Node.withValue(value);
        if (oldHead != null) {
            head.next = oldHead;
            oldHead.previous = head;
            tail = head;
        }
        size++;
        return head;
    }

    public Node<T> putOnTop(Node<T> node) {
        var oldHead = head;
        node.next = oldHead;
        oldHead.previous = node;
        head = node;
        size++;
        return node;
    }

    public Node<T> peek() {
        var oldHead = head;
        var newHead = oldHead.next;
        newHead.previous = null;
        head = newHead;
        size--;
        return oldHead;
    }

    public int size() {
        return size;
    }
}
