package ru.tinkoff.fintech.simple.cache.demo.cache;

import java.util.function.Consumer;
import java.util.function.Function;

public interface Cache<T> {

    T getOrElseCompute(String id, Function<String, ? extends T> function);

    void runThenInvalidate(String id, Runnable runnable);

    void consumeThenInvalidate(String id, Consumer<String> consumer);
}
