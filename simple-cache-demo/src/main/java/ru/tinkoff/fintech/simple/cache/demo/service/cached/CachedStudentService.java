package ru.tinkoff.fintech.simple.cache.demo.service.cached;

import org.springframework.stereotype.Component;
import ru.tinkoff.fintech.simple.cache.demo.cache.Cache;
import ru.tinkoff.fintech.simple.cache.demo.cache.SimpleCache;
import ru.tinkoff.fintech.simple.cache.demo.model.Student;
import ru.tinkoff.fintech.simple.cache.demo.service.StudentRepository;

@Component
public class CachedStudentService {

    private final Cache<Student> cache = new SimpleCache<>();
    private final StudentRepository studentRepository;

    public CachedStudentService(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    public void save(Student student) {
        studentRepository.save(student);
    }

    public void update(Student student) {
        var studentId = student.id().toString();
        cache.runThenInvalidate(studentId, () -> studentRepository.update(student));
    }

    public Student find(String id) {
        return cache.getOrElseCompute(id, studentRepository::find);
    }

    public void remove(String id) {
        cache.consumeThenInvalidate(id, studentRepository::remove);
    }
}
