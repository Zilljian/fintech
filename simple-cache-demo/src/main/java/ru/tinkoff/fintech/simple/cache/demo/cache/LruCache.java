package ru.tinkoff.fintech.simple.cache.demo.cache;


import ru.tinkoff.fintech.simple.cache.demo.cache.deque.LruDeque;
import ru.tinkoff.fintech.simple.cache.demo.cache.deque.Node;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;

public class LruCache<T> implements Cache<T>{

    private final LruDeque<T> queue = new LruDeque<>();
    private final Map<String, Node<T>> map = new HashMap<>();
    private final int capacity;

    public LruCache(int capacity) {
        this.capacity = capacity;
    }

    public synchronized T getOrElseCompute(String id, Function<String, ? extends T> function) {
        var node = map.get(id);
        if (node != null) {
            queue.remove(node);
            queue.putOnTop(node);
            return node.value;
        } else if (queue.size() == capacity) {
            map.remove(queue.removeLeastUsed());
        }
        var functionResult = function.apply(id);
        var newNode = queue.putOnTop(functionResult);
        map.put(id, newNode);
        return functionResult;
    }

    public synchronized void runThenInvalidate(String id, Runnable runnable) {
        var node = map.get(id);
        if (node != null) {
            queue.remove(node);
            map.remove(node);
        }
        runnable.run();
    }

    public synchronized void consumeThenInvalidate(String id, Consumer<String> consumer) {
        var node = map.get(id);
        if (node != null) {
            queue.remove(node);
            map.remove(node);
        }
        consumer.accept(id);
    }

    public int size() {
        return queue.size();
    }

    public int capacity() {
        return capacity;
    }
}
