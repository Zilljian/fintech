package ru.tinkoff.fintech.spring.mvc.demo.model;

import ru.tinkoff.fintech.spring.mvc.demo.validation.BoardGameConstraint;

@BoardGameConstraint
public record BoardGame(
    String name,
    String players,
    Integer playTime,
    Float rating,
    String description
) {}
