package ru.tinkoff.fintech.spring.mvc.demo.validation;

import ru.tinkoff.fintech.spring.mvc.demo.model.BoardGame;

import java.util.regex.Pattern;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

public class BoardGameValidator implements ConstraintValidator<BoardGameConstraint, BoardGame> {

    private final Pattern validPlayers = Pattern.compile("[1-9]-[2-9]");

    @Override
    public boolean isValid(BoardGame boardGame, ConstraintValidatorContext constraintValidatorContext) {
        if (isNull(boardGame.name()) || boardGame.name().isBlank()) {
            return false;
        } else {
            return nonNull(boardGame.players()) && validPlayers.matcher(boardGame.players()).matches();
        }
    }
}
