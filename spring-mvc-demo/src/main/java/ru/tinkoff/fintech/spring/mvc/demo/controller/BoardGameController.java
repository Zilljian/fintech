package ru.tinkoff.fintech.spring.mvc.demo.controller;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.tinkoff.fintech.spring.mvc.demo.exception.ApplicationError.ApplicationException;
import ru.tinkoff.fintech.spring.mvc.demo.exception.ApplicationError.ApplicationException.ApplicationExceptionCompanion;
import ru.tinkoff.fintech.spring.mvc.demo.model.BoardGame;

import java.util.HashMap;
import java.util.Map;
import javax.validation.Valid;

import static java.lang.String.format;
import static java.util.Objects.nonNull;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static ru.tinkoff.fintech.spring.mvc.demo.exception.ApplicationError.BOARD_GAME_ALREADY_EXISTS;

@OpenAPIDefinition(
    info = @Info(
        title = "board-game-service",
        version = "1.0.0",
        description = "Service provides board game operations",
        contact = @Contact(
            name = "Balakin Ilya",
            email = "i.balakin@tinkoff.ru"
        )
    )
)
@RestController
public class BoardGameController {

    private static final Logger log = LoggerFactory.getLogger(BoardGameController.class);

    private final Map<String, BoardGame> games = new HashMap<>();

    @PostMapping(
        consumes = APPLICATION_JSON_VALUE,
        path = "/boardgame/"
    )
    @Operation(
        description = "Add new board game to data base. Method checks name validity",
        summary = "Add new board game"
    )
    public void addBoardGame(@RequestBody @Valid BoardGame boardGame) {
        games.compute(boardGame.name(), (k, v) -> {
            if (nonNull(v)) {
                throw BOARD_GAME_ALREADY_EXISTS.exception(format("Board geme name = %s", k));
            }
            return boardGame;
        });
        log.info("New game with name {} has been added!", boardGame.name());
    }

    @GetMapping(
        produces = APPLICATION_JSON_VALUE,
        path = "/boardgame/"
    )
    @Operation(
        description = "Get board game from data base",
        summary = "Get board game"
    )
    public BoardGame getBoardGame(@RequestParam("name") String name) {
        return games.get(name);
    }

    @ExceptionHandler(ApplicationException.class)
    public ResponseEntity<ApplicationExceptionCompanion> handleApplicationException(ApplicationException e) {
        return ResponseEntity.status(e.companion.code()).body(e.companion);
    }
}
